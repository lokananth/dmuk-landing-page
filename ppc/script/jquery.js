﻿
// Carousel Banner Start
$(document).ready(function () {
    $("#owl-demo1").owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        autoPlay: true,
        lazyLoad: true,
        singleItem: true,
        rewindNav: true,
        itemsTablet: true
        // "singleItem:true" is a shortcut for:
        //                 items : 1, 
        //                 itemsDesktop : true,
        //                 itemsDesktopSmall : true,
        //                 itemsTablet: true,
        //                 itemsMobile : true

    });

});
// Carousel Banner End

// Terms
function toggle() {
    var ele = document.getElementById("toggleText");
    var text = document.getElementById("displayText");
    if (ele.style.display == "block") {
        ele.style.display = "none";
        text.className = "";
        text.className = "teamsconditions_pluss";
    }
    else {
        ele.style.display = "block";
        text.className = "";
        text.className = "teamsconditions_plusss";
    }
}
// Terms
  