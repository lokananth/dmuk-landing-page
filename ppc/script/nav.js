﻿$(function () {

    l = document.location.toString().toLowerCase();
    $('#leftNav a').each(function () {
        if (this.href.toLowerCase() == l) {
            $(this).parents('li').addClass('active');
            $(this).addClass('bold');
        }
    });
    $('#leftNav > li.active').height('auto');

});