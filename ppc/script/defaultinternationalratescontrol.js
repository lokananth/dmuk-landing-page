﻿var strdvSpecialOffersDefaultMainContent = $('#dvSpecialOffersSection').html();
function GetRatesForCountry(id, countryID) {
    $('.selectcountry-popup-js').css({ 'display': 'none' });
    $('#imgFlagImage').attr('src', $('#imagCountryFlag_' + id).attr('src'));
    $('#imgFlagImage').css({ 'display': '' });
    //$('#ucInternationalRatesControl_txtSearchCountry').val($('#imagCountryFlag_' + id).attr('alt'));
    $('#ucInternationalRatesControl_txtSearchCountry').val($('#lnkCountrySearch' + id).attr('title'));
    $('.search-clear').css({ 'display': 'block' });
    $('#main_div').css({ 'display': 'block' });

    var objDdlCountry = document.getElementById("ucInternationalRatesControl_ddlCountry");
    objDdlCountry.value = id;
    var objDdlCountryValue = objDdlCountry.value;

    if (objDdlCountryValue == "0") {
        document.getElementById("dvServiceRates").style.display = "none";
        return;
    }
    if (true) { }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/default.aspx/GetAutoRates",
        data: "{'CountryCode':'" + countryID + "','maths':'" + Math.random() + "'}",
        dataType: "json",
        success: function (list) {
            var listing = list.d.split('|');
            $('#divLandLine').text(listing[0])
            $('#divMobile').text(listing[1]);
            $('#divSMS').text(listing[2]);

            $('#divgiffgaffLandLine').text(listing[3])
            $('#divgiffgaffMobile').text(listing[4]);
            $('#divgiffgaffSMS').text(listing[5]);

            $('#divVodafoneLandLine').text(listing[6])
            $('#divVodafoneMobile').text(listing[7]);
            $('#divVodafoneSMS').text(listing[8]);

            $('#divO2LandLine').text(listing[9])
            $('#divO2Mobile').text(listing[10]);
            $('#divO2SMS').text(listing[11]);

            $('#divEELandLine').text(listing[12])
            $('#divEEMobile').text(listing[13]);
            $('#divEESMS').text(listing[14]);

            $('#divTescoLandLine').text(listing[15])
            $('#divTescoMobile').text(listing[16]);
            $('#divTescoSMS').text(listing[17]);

            $('#divLycaLandLine').text(listing[18])
            $('#divLycaMobile').text(listing[19]);
            $('#divLycaSMS').text(listing[20]);

            $('#divLebaraLandLine').text(listing[21])
            $('#divLebaraMobile').text(listing[22]);
            $('#divLebaraSMS').text(listing[23]);

            $('#divOrangeLandLine').text(listing[24])
            $('#divOrangeMobile').text(listing[25]);
            $('#divOrangeSMS').text(listing[26]);

            $('#divintrates').text(listing[27]);
        },
        error: function (list) {

        }
    });
}

function SetRatesForCountry() {
    if (xmlHttp.readyState == 4) {

        var objRates = xmlHttp.responseXML.getElementsByTagName('rates');
        var gblConuntryName = "";
        var objCount = getNodeValue(objRates[0], 'count');
        var objSpecialOfferCount = getNodeValue(objRates[0], 'specialoffercount');
        var dvRates = document.getElementById("dvServiceRates");
        if (objCount == "0") {
            dvRates.style.display = "none";
            return false;
        }
        else {
            var objCountryType = getNodeValue(objRates[0], 'countrytype');
            var objCountryName = getNodeValue(objRates[0], 'countryname');
            var objCountryImage = getNodeValue(objRates[0], 'countryimage');
            var flagDisplayLycaRate = getNodeValue(objRates[0], 'flagdisplaylycarate');
            var objConditionText = getNodeValue(objRates[0], 'conditiontext');
            var objServiceRates = xmlHttp.responseXML.getElementsByTagName('servicerates');

            var objDdlCountry = document.getElementById("ucInternationalRatesControl_ddlCountry");
            var objDdlCountryValue = objDdlCountry.value;
            gblConuntryName = objCountryName;
            var strRatePrefix = '';
            var strUnitPrefix = '';
            if (objCountryType == "1") {
                strRatePrefix = 'spn'; strUnitPrefix = 'spnUnit';
                document.getElementById('dvLycaCountryService').style.display = ''; document.getElementById('dvOtherCountryService').style.display = 'none';
            }
            else {
                strRatePrefix = 'spnO'; strUnitPrefix = 'spnUnitO';
                document.getElementById('dvLycaCountryService').style.display = 'none'; document.getElementById('dvOtherCountryService').style.display = '';
            }
            //strRatePrefix = 'spnO'; strUnitPrefix = 'spnUnitO';
            //document.getElementById('dvOtherCountryService').style.display = '';

            var objDivServiceRates = document.getElementById('dvServiceRates');
            if (objServiceRates.length > 0) {
                // For Desktop Version
                setRate(objServiceRates, 'landline', strRatePrefix, strUnitPrefix, 'LandLine');
                setRate(objServiceRates, 'mobile', strRatePrefix, strUnitPrefix, 'OtherMobile');
                setRate(objServiceRates, 'sms', strRatePrefix, strUnitPrefix, 'OtherMobileSMS');
                setRate(objServiceRates, 'lycamobile', strRatePrefix, strUnitPrefix, 'LycaMobile');
                setRate(objServiceRates, 'lycamobilesms', strRatePrefix, strUnitPrefix, 'LycaMobileSMS');
                objDivServiceRates.style.display = "";
                document.getElementById("dvConditionText").innerHTML = Encoder.htmlDecode(objConditionText);
            }
            else {
                objDivServiceRates.style.display = "none";
            }

            //Displaying special offer
            if (objSpecialOfferCount != '0') {
                var objSpecialOffers = xmlHttp.responseXML.getElementsByTagName('specialoffers');
                if (objSpecialOffers != null && objSpecialOffers.length > 0) {
                    var objSpecialOfferDetails = xmlHttp.responseXML.getElementsByTagName('specialofferdetail');
                    //alert(objSpecialOfferDetails.length);
                    if (objSpecialOfferDetails != null && objSpecialOfferDetails.length > 0) {
                        $('#dvSpecialOffersSection').html(strdvSpecialOffersDefaultMainContent);
                        $('#lycaOfferTo').html("");
                        //special offer header
                        $("#dvHeader").html($("#dvHeader").html() + " " + gblConuntryName);

                        for (var i = 0; i < objSpecialOfferDetails.length; i++) {
                            setSpecialOffer(objSpecialOfferDetails[i], i + 1);

                            if ((i + 1) == objSpecialOfferDetails.length) {
                                $("#dvSpecialOffersSection").css({ 'display': '' });
                                $("#lycaOfferTo").flexisel({
                                    visibleItems: 1,
                                    animationSpeed: 500,
                                    enableResponsiveBreakpoints: true,
                                    responsiveBreakpoints: {
                                        portrait: {
                                            changePoint: 480,
                                            visibleItems: 1
                                        },
                                        tablet: {
                                            changePoint: 768,
                                            visibleItems: 1
                                        }
                                    }
                                });

                            }
                        }
                        $(".nbs-flexisel-nav-left, .nbs-flexisel-nav-right").click(function () {
                            clearSelection();
                            $(".lyca-offer-box .readmore.active").click();
                        });
                        function clearSelection() {
                            if (document.selection) {
                                document.selection.empty();
                            } else if (window.getSelection) {
                                window.getSelection().removeAllRanges();
                            }
                        }

                        $("#dvSpecialOffersSection .readmore").click(function () {
                            $(this).toggleClass('active');
                            var thisBtn = $(this);
                            if ($(this).parents().hasClass("on") == false) {
                                $(".on").find(".readmore").click();
                            }
                            if (thisBtn.parents("li").attr("class") == "nbs-flexisel-item" || thisBtn.parents("li").attr("class") == "nbs-flexisel-item on") {
                                thisBtn.parents("li").toggleClass("on");
                            }
                            if (thisBtn.parents("div").hasClass("n-planbox") == true) {
                                thisBtn.parents(".n-planbox").toggleClass("on");
                            }

                            $(this).parent().parent().find(".plan-moreinfo").toggle("fast", function () { });
                            $(this).parent().parent().find(".offer-moreinfo").toggle("fast", function () { });
                            ($(this).text() == gblStrReadMore) ? $(this).text(gblStrReadLess) : $(this).text(gblStrReadMore);
                        });

                    }

                    // $("#lycaOffer").after("<div style='background: #FFF; min-height: 120px; width: 3px; position: absolute;'></div>");
                }
                else {
                    //     $("#dvSpecialOffersDefaultBanner").css({'display':''});
                }
            }
            else {
                $("#dvSpecialOffersSection").css({ 'display': 'none' });
                //   $("#dvSpecialOffersDefaultBanner").css({'display':''});
            }
            // Hide default case 
            //$('#dvDefaultCountryService').css({ "display": "none" });                   
            //$('#dvDefaultCountryService').removeClass('visible-desktop');
            return false;
        }
    }
}

function setRate(objServiceRates, strXMLTagName, strRatePrefix, strUnitPrefix, strHTMLTagName) {
    var objService = objServiceRates[0].getElementsByTagName(strXMLTagName);
    var strRate = getNodeValue(objService[0], 'rate');
    var strCondition = getNodeValue(objService[0], 'condition');
    var objRate = document.getElementById(strRatePrefix + strHTMLTagName);
    var objUnit = document.getElementById(strUnitPrefix + strHTMLTagName);

    if (objRate) {
        objRate.innerHTML = strRate;
        if (strRate == "Free") {
            objRate.innerHTML = gblStrFreeText;
        }
        if (strCondition != '') {
            objRate.innerHTML += "<sup>" + strCondition + "</sup>";
        }
        //if (strRate == "Free") { objRate.className = objRate.className + " dis-inlblk mrg15-T"; }
        //else { objRate.className = objRate.className.replace(" dis-inlblk mrg15-T", ""); }
    }
    if (objUnit) {
        if (strRate == "Free") { objUnit.style.display = "none"; }
        else { objUnit.style.display = ""; }
    }
}

Encoder.EncodeType = "entity";

function setSpecialOffer(objSpecialOffer, itemIndex) {
    var objDefaultSpecialOfferHTML = $('#offer-scroller-default').html();
    objDefaultSpecialOfferHTML = objDefaultSpecialOfferHTML.replace(/_0/g, "_" + itemIndex);
    //Adding into scroller

    // Hide Default Case 
    //  $('#dvDefaultCountryService').css({ "display": "none" });
    // $('#dvDefaultCountryService').removeClass('visible-desktop');
    //  $('#dvDefault').css({ "display": "none" });

    $('#lycaOfferTo').html($('#lycaOfferTo').html() + objDefaultSpecialOfferHTML);
    $('#spnSpecialOfferTitle_' + itemIndex).html(Encoder.htmlDecode(getNodeValue(objSpecialOffer, 'title')));
    $('#spnSpecialOfferSdesc_' + itemIndex).html(Encoder.htmlDecode(getNodeValue(objSpecialOffer, 'sdesc')))
    $('#lnkSpecialOfferLink_' + itemIndex).attr('href', Encoder.htmlDecode(getNodeValue(objSpecialOffer, 'seourl')).replace("[rooturl]", gblStrRootUrl));

}


/******************************************************************************************************************************************************/


$('.country-offer-dd .country-dd-open').click(function () {

    //Code for calling ajax here
    GetSearchCountry(this.event, 'Y');
});

$('.search-clear').click(function () {
    $('.search-clear').css({ 'display': 'none' });
});

function GetSearchCountry(e, flagIsRequestForAll) {
    var unicode;
    ///// CODE TO MOVE COUNTRY SELECTION UP/DOWN USING KEYBAORD ARROW KEY
    if (e == undefined || e == null) {
        unicode = 0;
    }
    else {
        unicode = e.keyCode ? e.keyCode : e.charCode;
    }

    if (unicode == 13) {
        //Do nothing				
    }
    else if (unicode == 38 || unicode == 40) {
        var minVal = 1;
        var maxVal = document.getElementById('hdnCntCountryId').value;

        // up key pressed
        if (unicode == 38) {

            selSpan = document.getElementById("hdnSelCountryId").value;
            selSpan--;
            document.getElementById("hdnSelCountryId").value = selSpan;

            if (selSpan < minVal) {
                selSpan = minVal;
                document.getElementById("hdnSelCountryId").value = selSpan;

            }

            if (selSpan > maxVal) {
                selSpan = maxVal;
                document.getElementById("hdnSelCountryId").value = selSpan;

            }

            var flDisp = true;
            var cnt = 1;
            while (flDisp) {

                var spanSrc = document.getElementById('lnkCountrySearch' + cnt);
                if (spanSrc != null) {

                    if (cnt == selSpan) {
                        spanSrc.className = "active-country";
                        $('#lnkCountrySearch' + cnt).trigger("hover");

                    }
                    else {
                        spanSrc.className = "";
                    }
                }
                else {
                    flDisp = false;
                }
                cnt++;
            }

            var liName = "lnkCountrySearch" + selSpan;
            if (document.getElementById(liName) != null) {
                var strVal = $("#lnkCountrySearch" + selSpan + " span").html();
                strVal = strVal.replace("&amp;", "&");
                document.getElementById(strCountrySearchTexboxId).value = strVal;
                document.getElementById('hdnDisSearchCountry').value = strVal;
            }
        }
        // down key pressed
        else if (unicode == 40) {
            selSpan = document.getElementById("hdnSelCountryId").value;
            selSpan++;

            document.getElementById("hdnSelCountryId").value = selSpan;
            if (selSpan < minVal) {
                selSpan = minVal;
                document.getElementById("hdnSelCountryId").value = selSpan;
            }

            if (selSpan > maxVal) {
                selSpan = maxVal;
                document.getElementById("hdnSelCountryId").value = selSpan;
            }

            var flDisp = true;
            var cnt = 1;
            while (flDisp) {
                var spanSrc = document.getElementById('lnkCountrySearch' + cnt);
                if (spanSrc != null) {
                    if (cnt == selSpan) {
                        spanSrc.className = 'active-country';
                        $('#lnkCountrySearch' + cnt).trigger("hover");
                    }
                    else {
                        spanSrc.className = '';
                    }
                }
                else {
                    flDisp = false;
                }
                cnt++;
            }
        }

        var liName = "lnkCountrySearch" + selSpan;
        if (document.getElementById(liName) != null) {
            var strVal = $("#lnkCountrySearch" + selSpan + " span").html();
            strVal = strVal.replace("&amp;", "&");
            document.getElementById(strCountrySearchTexboxId).value = strVal;
            document.getElementById('hdnDisSearchCountry').value = strVal;
        }
    }

    else {
        callCountrys($('#ucInternationalRatesControl_txtSearchCountry').val());


    }
}


function callCountrys(countryName) {
    var path = window.location.pathname;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/default.aspx/GetAutoCompleteData",
        data: "{'username':'" + countryName + "','path':'" + path + "','maths':'" + Math.random() + "'}",
        dataType: "json",
        success: function (list) {
            $('#dvSearchResultForCountry').css('display', 'block');
            $('#dvSearchResultForCountry').html(list.d);
            SetSearchCountry(list.d);

        }
    });
}

var varFirstCountryId = "";
function SetSearchCountry(values) {
    $('#spnClientEventsReset').css({ 'display': 'none' });
    $('#dvSearchResultForCountry').html(values);
    $('#hdnSelCountryId').val('0');
    $('#hdnDisSearchCountry').val("");
    $('#hdnCntCountryId').val($('#dvSearchResultForCountry').children().length);
    $('.selectcountry-popup-js').css({ 'display': 'block' });
}

function ClearSelection() {
    $('#ucInternationalRatesControl_txtSearchCountry').val(gblStrIntialValue);
    $('#imgFlagImage').attr('src', '');
    $('#imgFlagImage').css({ 'display': 'none' });
    $('.search-clear').css({ 'display': 'none' });
    $('#dvSpecialOffersSection, #dvServiceRates').css({ 'display': 'none' });

}


function checkKey(event) {
    code = (event.keyCode ? event.keyCode : event.which);
    if (code == 13) {
    }
}


function setDropdownCss(action, id, text) {
    var objText = document.getElementById(id);
    if (action == 'set') {
        if (!$('.country-offer-dd').hasClass('open')) {
            $('.country-offer-dd').addClass('open');
        }
        if (objText.value == text) { objText.value = ''; }
    }
    else {
        if (objText.value == '') { objText.value = text; }
    }
}

$(document).ready(function () {
    $('#dvSearchResultForCountry').mouseover(function () {
        $('#dvSearchResultForCountry a.active-country').removeClass('active-country');
    });
});


$("#ucInternationalRatesControl_txtSearchCountry").keypress(function (event) {
    if (event.which == 13) {
        var objSelectedCountry = document.getElementById("hdnSelCountryId").value;
        if (objSelectedCountry == "0") {
            $("#dvSearchResultForCountry a:first").click();
            $(this).blur();
        }
        else {
            $("#dvSearchResultForCountry a.active-country").click();
            $(this).blur();
            $("#dvSearchResultForCountry a.active-country").removeClass('active-country');
        }
    }
});