var _txtPostCode;
var _txtHouseNo;
var _txtStreet;
var _txtTown;
var _btnFind;
var _ddlAddress;
var _divList;
var _lblFind;
var _lblChange;
var _lblSearch;

function PostCodeAnywhere_Init(txtPostCode, txtHouseNo, txtStreet, txtTown, btnFind, ddlAddress, divList, lblFind, lblChange, lblSearch) {
    _txtPostCode = document.getElementById(txtPostCode);
    _txtHouseNo = document.getElementById(txtHouseNo);
    _txtStreet = document.getElementById(txtStreet);
    _txtTown = document.getElementById(txtTown);
    _btnFind = document.getElementById(btnFind);
    _ddlAddress = document.getElementById(ddlAddress);
    _divList = document.getElementById(divList);
    _lblFind = lblFind;
    _lblChange = lblChange;
    _lblSearch = lblSearch;

    var reChoose = _txtPostCode.value != '' && _txtHouseNo.value != '' && _txtStreet.value != '' && _txtTown.value != '';

    $(_btnFind).val(!reChoose ? _lblFind : _lblChange);
    _txtPostCode.readOnly = reChoose;
    _txtHouseNo.readOnly = !reChoose;
    _txtStreet.readOnly = !reChoose;
    _txtTown.readOnly = !reChoose;

    $(_btnFind).click(function () {
        AddressListBegin('KG22-JN94-EF64-MW54', _txtPostCode.value);
    });
}

function AddressListBegin(Key, Postcode) {
    // Change interface
    if (_btnFind.value == _lblChange) {
        _btnFind.value = _lblFind;

        _txtPostCode.focus();
        _txtPostCode.select();

        _txtHouseNo.value = '';
        _txtStreet.value = '';
        _txtTown.value = '';

        _txtPostCode.readOnly = false;
        _txtHouseNo.readOnly = true;
        _txtStreet.readOnly = true;
        _txtTown.readOnly = true;
        return;
    }

    // Cannot proceed if postcode empty
    if (Postcode == undefined || Postcode == '') {
        alert('Please provide a postcode!');
        return;
    }

    _btnFind.value = _lblSearch;
    var scriptTag = document.getElementById("pcascript");
    var headTag = document.getElementsByTagName("head").item(0);
    var strUrl = "";
    // Build the url
    strUrl = "https://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/FindByPostcode/v1.00/json.ws?";
    strUrl += "&Key=" + escape(Key);
    strUrl += "&Postcode=" + escape(Postcode);
    strUrl += "&UserName='SWITC11123'";
    strUrl += "&CallbackFunction=AddressListEnd";
    // Make the request
    if (scriptTag) try { headTag.removeChild(scriptTag) } catch (e) { }
    scriptTag = document.createElement("script");
    scriptTag.src = strUrl;
    scriptTag.type = "text/javascript";
    scriptTag.id = "pcascript";
    headTag.appendChild(scriptTag);
}

function AddressListEnd(response) {
    if (response.length == 1 && typeof (response[0].Error) != 'undefined')
    { alert(response[0].Description); }
    else {
        if (response.length == 0) alert("Sorry, no matching items found");
        else {
            _ddlAddress.options.length = 0;
            _ddlAddress.options[0] = new Option("0", "Please Select");
            for (var i in response)
                _ddlAddress.options[_ddlAddress.options.length] = new Option(response[i].StreetAddress, response[i].Id);
            _ddlAddress.onchange = function () { SelectAddress('KG22-JN94-EF64-MW54', this.value); }
            _divList.style.display = "";
        }
    }
    _btnFind.value = _lblFind;
}

function SelectAddress(Key, Id) {
    if (!Id) return;
    var scriptTag = document.getElementById("pcascript");
    var headTag = document.getElementsByTagName("head").item(0);
    var strUrl = "";
    // Build the url
    strUrl = "https://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/RetrieveById/v1.00/json.ws?";
    strUrl += "&Key=" + escape(Key);
    strUrl += "&Id=" + escape(Id);
    strUrl += "&UserName='SWITC11123'";
    strUrl += "&CallbackFunction=SelectAddressEnd";
    // Make the request
    if (scriptTag) try { headTag.removeChild(scriptTag) } catch (e) { }
    scriptTag = document.createElement("script");
    scriptTag.src = strUrl;
    scriptTag.type = "text/javascript";
    scriptTag.id = "pcascript";
    headTag.appendChild(scriptTag);
    // User interface changed
    _btnFind.value = _lblChange;
    _txtPostCode.readOnly = true;
    _txtHouseNo.readOnly = false;
    _txtStreet.readOnly = false;
    _txtTown.readOnly = false;
}

function SelectAddressEnd(response) {
    if (response.length == 1 && typeof (response[0].Error) != 'undefined') {
        alert(response[0].Description);
    }
    else {
        if (response.length == 0)
            alert("Sorry, no matching items found");
        else {
            var houseNo = '';
            var street = '';
            if (response[0].Line3 != '') {
                houseNo = response[0].Line1 + ', ' + response[0].Line2;
                street = response[0].Line3;
            }
            else if (response[0].Line2 != '') {
                houseNo = response[0].Line1;
                street = response[0].Line2;
            }
            else {
                var line1 = response[0].Line1.split(" ");
                houseNo = line1[0];
                for (i = 1; i < line1.length; i++) street += line1[i] + " ";
            }

            _divList.style.display = "none";
            _txtHouseNo.value = houseNo;
            _txtStreet.value = street;
            _txtTown.value = response[0].PostTown;
        }
    }
}
