<?php if(!isset($_SESSION)){session_start();} ?>
<!--     **********ppc validation start*************** -->

    <script src="../ppc/js/dmukvalidation.js" type="text/javascript"></script>

<!--  **********ppc validation end***************  -->
<div class="lpright">
                
<div class="freesimbox1">
    
  <script type="text/javascript">
        $(document).ready(function () {

            var val = document.getElementById('dmuklp_TextBoxHouseNumber');
            if (val.value != "") {
                document.getElementById('dmuklp_TextBoxHouseNumber').style.display = "block";
                document.getElementById('dmuklp_TextBoxStreet').style.display = "block";
                document.getElementById('dmuklp_TextBoxStreet1').style.display = "block";
                document.getElementById('dmuklp_TextBoxTown').style.display = "block";
                document.getElementById('dmuklp_divaddressfields').style.display = "block";

                document.getElementById('dmuklp_TextBoxHouseNumber').readOnly = true;
                document.getElementById('dmuklp_TextBoxStreet').readOnly = true;
                document.getElementById('dmuklp_TextBoxTown').readOnly = true;

            }

            (function (n) {
                "use strict";

                function t(t) {
                    var r, i;
                    this.config = {};
                    n.extend(this, u);
                    t && n.extend(this, t);
                    r = {};
                    for (i in this.output_fields) this.output_fields[i] !== undefined && (r[i] = n(this.output_fields[i]));
                    this.$output_fields = r
                }
                var i = [],
        r, u = {
            api_key: "",
            output_fields: {
                line_1: "#line1",
                line_2: "#line2",
                line_3: "#line3",
                post_town: "#town",
                postcode: "#postcode",
                postcode_inward: undefined,
                postcode_outward: undefined,
                udprn: undefined,
                dependant_locality: undefined,
                double_dependant_locality: undefined,
                thoroughfare: undefined,
                dependant_thoroughfare: undefined,
                building_number: undefined,
                building_name: undefined,
                sub_building_name: undefined,
                po_box: undefined,
                department_name: undefined,
                organisation_name: undefined,
                postcode_type: undefined,
                su_organisation_indicator: undefined,
                delivery_point_suffix: undefined
            },
            api_endpoint: "https://api.getAddress.io/uk",
            input: undefined,
            $input: undefined,
            input_label: "Enter your Postcode",
            input_muted_style: "color:#CBCBCB;",
            input_class: "",
            input_id: "opc_input",
            button: undefined,
            $button: undefined,
            button_id: "opc_button",
            button_label: "Find address",
            button_class: "",
            button_disabled_message: "Find address",
            $dropdown: undefined,
            dropdown_id: "opc_dropdown",
            dropdown_select_message: "Select your Address",
            dropdown_class: "",
            $error_message: undefined,
            error_message_id: "opc_error_message",
            error_message_postcode_invalid: "Please recheck your postcode, it seems to be incorrect",
            error_message_postcode_not_found: "Your postcode could not be found. Please type in your address",
            error_message_default: "We were not able to your address from your Postcode. Please input your address manually",
            error_message_class: "",
            lookup_interval: 1e3,
            debug_mode: !1,
            onLookupSuccess: undefined,
            onLookupError: undefined,
            onAddressSelected: undefined
        };
                t.prototype.setupPostcodeInput = function (n) {
                    this.$context = n;
                    this.setupInputField();
                    this.setupLookupButton()
                };
                t.prototype.setupInputField = function () {
                    var t = this;
                    return this.$input = n(this.input).length ? n(this.input).first() : n("<input />", {
                        type: "text",
                        id: this.input_id,
                        value: this.input_label,
                        maxlength: 8
                    }).appendTo(this.$context).addClass(this.input_class).val(this.input_label).attr("style", this.input_muted_style).attr("autocomplete", "off").submit(function () {
                        return !1
                    }).keypress(function (n) {
                        n.which === 13 && t.$button.trigger("click")
                    }).focus(function () {
                        t.$input.removeAttr("style").val("")
                    }).blur(function () {
                        t.$input.val() || (t.$input.val(t.input_label), t.$input.attr("style", t.input_muted_style))
                    }), this.$input
                };
                t.prototype.setupLookupButton = function () {
                    var t = this;
                    return this.$button = n(this.button).length ? n(this.button).first() : n("<button />", {
                        html: this.button_label,
                        id: this.button_id,
                        type: "button"
                    }).appendTo(this.$context).addClass(this.button_class).attr("onclick", "return false;").submit(function () {
                        return !1
                    }), this.$button.click(function () {
                        if ($('#opc_button').text() == "Change") {
                            document.getElementById("dmuklp_divaddressfields").style.display = "none";
                            document.getElementById("opc_dropdown").style.display = "block";
                            var n = t.$input.val();
                            t.disableLookup();
                            t.clearAll();
                            t.lookupPostcode(n)
                            $("#opc_button").html('Find your Address');
                        }
                        else {
                            var n = t.$input.val();
                            t.disableLookup();
                            t.clearAll();
                            t.lookupPostcode(n)
                        }

                    }), this.$button
                };

                t.prototype.disableLookup = function (n) {
                    n = n || this.button_disabled_message;
                    this.$button.prop("disabled", !0).html(n)
                };
                t.prototype.enableLookup = function () {
                    var n = this;
                    n.lookup_interval === 0 ? n.$button.prop("disabled", !1).html(n.button_label) : setTimeout(function () {
                        n.$button.prop("disabled", !1).html(n.button_label)
                    }, n.lookup_interval)
                };
                t.prototype.clearAll = function () {
                    this.setDropDown();
                    this.setErrorMessage();
                    this.setAddressFields()
                };
                t.prototype.removeAll = function () {
                    this.$context = null;
                    n.each([this.$input, this.$button, this.$dropdown, this.$error_message], function (n, t) {
                        t && t.remove()
                    })
                };
                t.prototype.lookupPostcode = function (t) {
                    var i = this;
                    if (!n.getAddress.validatePostcodeFormat(t)) return this.enableLookup(), i.setErrorMessage(this.error_message_postcode_invalid);
                    n.getAddress.lookupPostcode(t, i.api_key, function (n) {
                        if (i.enableLookup(), i.setDropDown(n.Addresses, t), i.onLookupSuccess) i.onLookupSuccess(n)
                    }, function (n) {
                        n.status == 404 ? i.setErrorMessage(i.error_message_postcode_not_found) : i.setErrorMessage("Unable to connect to server");
                        i.enableLookup();
                        i.onLookupError && i.onLookupError()
                    })
                };
                t.prototype.setDropDown = function (t, i) {
                    var r = this,
            u, e, f;
                    if (this.$dropdown && this.$dropdown.length && (this.$dropdown.remove(), delete this.$dropdown), t) {
                        for (u = n("<select />", {
                            id: r.dropdown_id
                        }).addClass(r.dropdown_class), n("<option />", {
                            value: "open",
                            text: r.dropdown_select_message
                        }).appendTo(u), e = t.length, f = 0; f < e; f += 1) n("<option />", {
                            value: f,
                            text: t[f]
                        }).appendTo(u);
                        return u.appendTo(r.$context).change(function () {
                            var u = n(this).val();
                            u >= 0 && (r.setAddressFields(t[u], i), r.onAddressSelected && r.onAddressSelected.call(this, t[u]))
                            document.getElementById("opc_dropdown").style.display = "none";
                            document.getElementById("dmuklp_divaddressfields").style.display = "block";
                            document.getElementById("dmuklp_TextBoxHouseNumber").readOnly = true;
                            document.getElementById("dmuklp_TextBoxStreet").readOnly = true;
                            document.getElementById("dmuklp_TextBoxStreet1").readOnly = true;
                            document.getElementById("dmuklp_TextBoxTown").readOnly = true;
                            $("#opc_button").html('Change');
                        }), r.$dropdown = u, u
                    }
                };
                t.prototype.setErrorMessage = function (t) {
                    if (this.$error_message && this.$error_message.length && (this.$error_message.remove(), delete this.$error_message), t) return this.$error_message = n("<p />", {
                        html: t,
                        id: this.error_message_id
                    }).addClass(this.error_message_class).appendTo(this.$context), this.$error_message
                };
                t.prototype.setAddressFields = function (n, t) {
                    var f, r, u, i;
                    for (f in this.$output_fields) this.$output_fields[f].val("");
                    if (n) {
                        for (r = n.split(","), u = r.length, i = 0; i < u; i++) i == 0 ? this.$output_fields.line_1.val(r[i].trim() || "") : i + 1 == u ? this.$output_fields.post_town.val(r[i].trim() || "") : i == 1 ? this.$output_fields.line_2.val(r[i].trim() || "") : i == 2 && this.$output_fields.line_3.val(r[i].trim() || "");
                        t && (t = t.toUpperCase().trim());
                        this.$output_fields.postcode.val(t || "")
                    }
                };
                n.getAddress = {
                    defaults: function () {
                        return u
                    },
                    setup: function (n) {
                        r = new t(n);
                        i.push(r)
                    },
                    validatePostcodeFormat: function (n) {
                        return !!n.match(/^[a-zA-Z0-9]{1,4}\s?\d[a-zA-Z]{2}$/)
                    },
                    lookupPostcode: function (t, i, r, f) {
                        var o = u.api_endpoint,
                s = [o, t].join("/"),
                e = {
                    url: s,
                    data: {
                        "api-key": i
                    },
                    dataType: "json",
                    timeout: 3e5,
                    success: r
                };
                        f && (e.error = f);
                        n.ajax(e)
                    },
                    clearAll: function () {

                        for (var t = i.length, n = 0; n < t; n += 1) i[n].removeAll()
                    }
                };
                n.fn.getAddress = function (u) {
                    if (u) {
                        var f = new t(u);
                        i.push(f);
                        f.setupPostcodeInput(n(this))
                    } else r.setupPostcodeInput(n(this));
                    return this
                }
            })(jQuery);

            $('#postcode_lookup').getAddress({
                api_key: '-PTg0SMM0EaWNMmiTz4gzw748',
                output_fields: {
                    line_1: '#dmuklp_TextBoxHouseNumber',
                    line_2: '#dmuklp_TextBoxStreet',
                    line_3: '#dmuklp_TextBoxStreet1',
                    post_town: '#dmuklp_TextBoxTown',
                    postcode: '#dmuklp_TextBoxPostCode'
                }

            });

        });

        //        function potong(str) {
        //            if (!str || typeof str != 'string')
        //                return null;
        //            return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
        //        }

        //        function AddressListBegin(Key, Postcode) {
        //            // Change interface
        //            var elemBtnFind = document.getElementById('btnFind');
        //            if (elemBtnFind.value == 'Change') {
        //                elemBtnFind.value = 'Find Address';

        //                document.getElementById('dmuklp_TextBoxPostCode').focus();
        //                document.getElementById('dmuklp_TextBoxPostCode').select();

        //                document.getElementById('dmuklp_TextBoxHouseNumber').value = '';
        //                document.getElementById('dmuklp_TextBoxStreet').value = '';
        //                document.getElementById('dmuklp_TextBoxTown').value = '';

        //                document.getElementById("dmuklp_divaddressfields").style.display = "none";
        //                document.getElementById("divAddressTextbox").style.display = "block";
        //                document.getElementById('dmuklp_TextBoxPostCode').readOnly = false;
        //                document.getElementById('dmuklp_TextBoxHouseNumber').readOnly = true;
        //                document.getElementById('dmuklp_TextBoxStreet').readOnly = true;
        //                document.getElementById('dmuklp_TextBoxTown').readOnly = true;

        //                return;
        //            }

        //            var scriptTag = document.getElementById("pcascript");
        //            var headTag = document.getElementsByTagName("head").item(0);
        //            var strUrl = "";
        //            // Build the url
        //            strUrl = "https://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/FindByPostcode/v1.00/json.ws?";
        //            strUrl += "&Key=" + escape(Key);
        //            strUrl += "&Postcode=" + escape(Postcode);
        //            strUrl += "&UserName='SWITC11123'";
        //            strUrl += "&CallbackFunction=AddressListEnd";
        //            // Make the request
        //            if (scriptTag) try { headTag.removeChild(scriptTag) } catch (e) { }
        //            scriptTag = document.createElement("script");
        //            scriptTag.src = strUrl;
        //            scriptTag.type = "text/javascript";
        //            scriptTag.id = "pcascript";
        //            headTag.appendChild(scriptTag);
        //        }

        //        function AddressListEnd(response) {
        //            if (response.length == 1 && typeof (response[0].Error) != 'undefined') {
        //                //alert(response[0].Description);
        //                if (response[0].Description == "Postcode Invalid") {
        //                    $('#dmuklp_divPostCode').removeClass("empty");
        //                    $('#dmuklp_divPostCode').addClass("block errormsg clear");
        //                    $('#dmuklp_divPostCode').html('Please enter a valid postcode.');
        //                }
        //                else {
        //                    $('#dmuklp_divPostCode').removeClass("empty");
        //                    $('#dmuklp_divPostCode').addClass("block errormsg clear");
        //                    $('#dmuklp_divPostCode').html('Please enter a postcode.');
        //                }
        //            }
        //            else {
        //                if (response.length == 0) {
        //                    //alert("Sorry, no matching items found");
        //                    $('#dmuklp_divPostCode').removeClass("empty");
        //                    $('#dmuklp_divPostCode').addClass("block errormsg clear");
        //                    $('#dmuklp_divPostCode').html('Please enter a valid postcode.');
        //                }
        //                else {
        //                    var lstAddressIP = document.getElementById("lstAddressIP");
        //                    lstAddressIP.options.length = 0;
        //                    lstAddressIP.options[0] = new Option("Select Address", "");
        //                    for (var i in response)
        //                        lstAddressIP.options[lstAddressIP.options.length] = new Option(response[i].StreetAddress, response[i].Id);
        //                    lstAddressIP.onchange = function () { SelectAddress('KG22-JN94-EF64-MW54', this.value); }
        //                    document.getElementById("divAddressLabel").style.display = "";
        //                    document.getElementById("divAddressTextbox").style.display = "";
        //                }
        //            }
        //        }

        //        function SelectAddress(Key, Id) {
        //            if (!Id) return;
        //            var scriptTag = document.getElementById("pcascript");
        //            var headTag = document.getElementsByTagName("head").item(0);
        //            var strUrl = "";
        //            // Build the url
        //            strUrl = "https://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/RetrieveById/v1.00/json.ws?";
        //            strUrl += "&Key=" + escape(Key);
        //            strUrl += "&Id=" + escape(Id);
        //            strUrl += "&UserName='SWITC11123'";
        //            strUrl += "&CallbackFunction=SelectAddressEnd";
        //            // Make the request
        //            if (scriptTag) try { headTag.removeChild(scriptTag) } catch (e) { }
        //            scriptTag = document.createElement("script");
        //            scriptTag.src = strUrl;
        //            scriptTag.type = "text/javascript";
        //            scriptTag.id = "pcascript";
        //            headTag.appendChild(scriptTag);
        //            // User interface changed
        //            document.getElementById('btnFind').value = 'Change';
        //            document.getElementById('dmuklp_TextBoxPostCode').readOnly = true;
        //            document.getElementById('dmuklp_TextBoxHouseNumber').readOnly = false;
        //            document.getElementById('dmuklp_TextBoxStreet').readOnly = false;
        //            document.getElementById('dmuklp_TextBoxTown').readOnly = false;
        //        }

        //        function SelectAddressEnd(response) {
        //            if (response.length == 1 && typeof (response[0].Error) != 'undefined') {
        //                //alert(response[0].Description);
        //                if (response[0].Description == "Postcode Invalid") {
        //                    $('#dmuklp_divPostCode').removeClass("empty");
        //                    $('#dmuklp_divPostCode').addClass("block errormsg clear");
        //                    $('#dmuklp_divPostCode').html('Please enter a valid postcode.');
        //                }
        //                else {
        //                    $('#dmuklp_divPostCode').removeClass("empty");
        //                    $('#dmuklp_divPostCode').addClass("block errormsg clear");
        //                    $('#dmuklp_divPostCode').html('Please enter a postcode.');
        //                }
        //            }
        //            else {
        //                if (response.length == 0) {
        //                    //alert("Sorry, no matching items found");
        //                    $('#dmuklp_divPostCode').removeClass("empty");
        //                    $('#dmuklp_divPostCode').addClass("block errormsg clear");
        //                    $('#dmuklp_divPostCode').html('Please enter a valid postcode.');
        //                }
        //                else {
        //                    document.getElementById("divAddressLabel").style.display = "none";
        //                    document.getElementById("divAddressTextbox").style.display = "none";
        //                    document.getElementById("dmuklp_divaddressfields").style.display = "block";
        //                    var hno = response[0].Line1.split(" ");
        //                    var street = "";
        //                    if (potong(hno[0]) == "Flat" || potong(hno[0]) == "Apartment" || potong(hno[0]) == "Unit") {
        //                        document.getElementById('dmuklp_TextBoxHouseNumber').value = potong(hno[0] + " " + hno[1]);
        //                        for (i = 2; i < hno.length; i++)
        //                            street += hno[i] + " ";
        //                    }
        //                    else {
        //                        document.getElementById('dmuklp_TextBoxHouseNumber').value = potong(hno[0]);
        //                        for (i = 1; i < hno.length; i++)
        //                            street += hno[i] + " ";
        //                    }

        //                    street = street + " " + response[0].Line2 + " " + response[0].Line3 + response[0].Line4 + " " + response[0].Line5;
        //                    document.getElementById('dmuklp_TextBoxStreet').value = potong(street);
        //                    document.getElementById('dmuklp_TextBoxTown').value = potong(response[0].PostTown);

        //                    document.getElementById('dmuklp_TextBoxHouseNumber').readOnly = true;
        //                    document.getElementById('dmuklp_TextBoxStreet').readOnly = true;
        //                    document.getElementById('dmuklp_TextBoxTown').readOnly = true;
        //                }
        //            }
        //        }

    </script>
    
    <script type="text/javascript">
        $(document).ready(function () {
			
			 $('#dmuklp_txtFirstName,#dmuklp_txtLastName').bind('keypress keyup blur',function(){ 
            var node = $(this);
            node.val(node.val().replace(/[^a-z A-Z]/g,'') ); 
            });

            //title
            $('#dmuklp_ddlTitle').change(function () {
                var title = $('#dmuklp_ddlTitle').val();
                if (title != 0) {
                    $('#dmuklp_divTitle').addClass("empty");
                    $('#dmuklp_divTitle').removeClass("block errormsg clear");
                    $('#dmuklp_divTitle').text('');
                }
                else {
                    $('#dmuklp_divTitle').removeClass("empty");
                    $('#dmuklp_divTitle').addClass("block errormsg clear");
                    $('#dmuklp_divTitle').html('Please select a title.');
                }
            });

            //FirstName
            $('#dmuklp_txtFirstName').blur(function () {
                var Fristname = $('#dmuklp_txtFirstName').val();
                if (Fristname.length > 0) {
                    $('#dmuklp_divFirstName').addClass("empty");
                    $('#dmuklp_divFirstName').removeClass("block errormsg");
                    $('#dmuklp_divFirstName').text('');
                }
                else {
                    $('#dmuklp_divFirstName').removeClass("empty");
                    $('#dmuklp_divFirstName').addClass("block errormsg");
                    $('#dmuklp_divFirstName').html('Please enter a first name.');
                }
            });

            //LastName
            $('#dmuklp_txtLastName').blur(function () {
                var lastname = $('#dmuklp_txtLastName').val();
                if (lastname.length > 0) {
                    $('#dmuklp_divLastName').addClass("empty");
                    $('#dmuklp_divLastName').removeClass("block errormsg");
                    $('#dmuklp_divLastName').text('');
                }
                else {
                    $('#dmuklp_divLastName').removeClass("empty");
                    $('#dmuklp_divLastName').addClass("block errormsg");
                    $('#dmuklp_divLastName').html('Please enter a last name.');
                }
            });

            //Email ID
             $('#dmuklp_txtEmail').bind('keypress  keyup blur',function(){ 
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                var EmailID = $('#dmuklp_txtEmail').val();
                if (EmailID.length > 0) {
                    if (reg.test(EmailID)) {
                        $('#dmuklp_divEmail').addClass("empty");
                        $('#dmuklp_divEmail').removeClass("block errormsg");
                        $('#dmuklp_divEmail').text('');
                    }
                    else {
                        $('#dmuklp_divEmail').removeClass("empty");
                        $('#dmuklp_divEmail').addClass("block errormsg");
                        $('#dmuklp_divEmail').html('Please enter a valid  email address.');
                    }
                }
                else {
                    $('#dmuklp_divEmail').removeClass("empty");
                    $('#dmuklp_divEmail').addClass("block errormsg");
                    $('#dmuklp_divEmail').html('Please enter an email address.');
                }
            });

            //PostalCode
            $('#dmuklp_TextBoxPostCode').blur(function () {
                var PostCode = $('#dmuklp_TextBoxPostCode').val();
                if (PostCode.length > 0) {
                    $('#dmuklp_divPostCode').addClass("empty");
                    $('#dmuklp_divPostCode').removeClass("block errormsg clear");
                    $('#dmuklp_divPostCode').text('');
                }
                else {
                    $('#dmuklp_divPostCode').removeClass("empty");
                    $('#dmuklp_divPostCode').addClass("block errormsg clear");
                    $('#dmuklp_divPostCode').html('Please enter a postcode.');
                }
            });

        });
    </script>
    <script type="text/javascript">
        function init_Step2_radio() {
            $('.delightfreesim li').click(function () {
                $('.delightfreesim li').removeClass('active2');
                $(this).find('input:radio').attr('checked', 'checked');
                $('#dmuklp_divSimType').addClass("empty");
                $('#dmuklp_divSimType').removeClass("block errormsg clear");
                $('#dmuklp_divSimType').text('');
                $(this).addClass('active2');
            });
            $('.delightfreesim li input:checked').parents('li').addClass('active2');
        }
    </script>
    <script type="text/javascript">
        function init_Step1() {
            $('.freestepOne li').click(function () {
                $('.freestepOne li').removeClass('active');
                $(this).find('input:radio').attr('checked', 'checked');

                $(this).addClass('active');
            });
            $('.freestepOne li input:checked').parents('li').addClass('active');
        }
    </script>
    <script type="text/javascript">
        function beginRequestHandler() {
            vmuk.disable($('.actionSubmit'));
        }

        $(document).ready(function () {
            init_Step1();
            init_Step2_radio();
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandler);
        });
    </script>
    <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('dmuklp$srcFree', 'form1', [], [], [], 90, '');
//]]>
</script>

    <input type="hidden" value="Find address" id="dmuklp_btnFindText" name="dmuklp$btnFindText">
    <input type="hidden" value="Change" id="dmuklp_btnSearch" name="dmuklp$btnSearch">
    <input type="hidden" value="Change" id="dmuklp_btnChange" name="dmuklp$btnChange">
    <h4 class="h1">
        Order Free SIM</h4>
    <div id="dmuklp_diverrmsg">
    </div>
    <div class="common_field_main">
        <ul onclick="init_Step2_radio();" class="delightfreesim" id="dmuklp_rbSIMType">
	<li class="active2"><span data-img=""><input type="radio" checked="checked" value="1" name="dmuklp$" id="dmuklp_rbSIMType_0"><label for="dmuklp_rbSIMType_0">Standard/Micro SIM</label></span></li>
	<li><span data-img=""><input type="radio" value="2" name="dmuklp$" id="dmuklp_rbSIMType_1"><label for="dmuklp_rbSIMType_1">Nano SIM</label></span></li>

</ul>
    </div>
    <div id="dmuklp_divSimType">
    </div>
    <div style="color:red; font-size:14px;"><?php if((isset($_SESSION['status_message']))&&($_SESSION['status_message']!='')){echo $_SESSION['status_message'];} ?></div>
        <div><?php session_unset(); ?></div>
    <div style="display: none" class="common_field_main">
        <div class="top_label">
            Title</div>
        <div class="top_input">
            <select class="input_value1" id="dmuklp_ddlTitle" name="dmuklp$ddlTitle">
	<option value="0">Select title</option>
	<option value="Mr">Mr</option>
	<option value="Mrs">Mrs</option>
	<option value="Ms">Ms</option>
	<option value="Dr">Dr</option>
	<option value="Prof">Prof</option>
	<option value="Sir">Sir</option>
	<option value="Lady">Lady</option>
	<option value="Rev">Rev</option>
	<option value="Miss">Miss</option>
	<option value="Lord">Lord</option>
	<option value="Other">Other</option>

</select>
            <div id="dmuklp_divTitle">
            </div>
        </div>
    </div>
    <div class="common_field_main">
        <div class="top_label">
            First Name
        </div>
        <div class="top_input">
            <input type="text" onkeypress="doClick('dmuklp_btnProceed',event)" placeholder="Enter first name" autocomplete="Off" class="input_value" id="dmuklp_txtFirstName" name="dmuklp$txtFirstName">
            <div id="dmuklp_divFirstName">
            </div>
            
        </div>
    </div>
    <div class="common_field_main">
        <div class="top_label">
            Last Name
        </div>
        <div class="top_input">
            <input type="text" onkeypress="doClick('dmuklp_btnProceed',event)" autocomplete="Off" placeholder="Enter last name" class="input_value" id="dmuklp_txtLastName" name="dmuklp$txtLastName">
            <div id="dmuklp_divLastName">
            </div>
            
        </div>
    </div>
    <div style="display: none" class="common_field_main">
        <div class="top_label">
            Contact number (Optional)
        </div>
        <div class="top_input">
            <input type="text" class="input_value" placeholder="Enter contact number" autocomplete="Off" id="dmuklp_txtmobileno" maxlength="15" name="dmuklp$txtmobileno">
            <div id="dmuklp_divmobileno">
            </div>
            
        </div>
    </div>
    <div class="common_field_main">
        <div class="top_label">
            Email Address
        </div>
        <div class="top_input">
            <input type="text" onkeypress="doClick('dmuklp_btnProceed',event)" class="input_value" placeholder="Enter e-mail address" autocomplete="Off" id="dmuklp_txtEmail" name="dmuklp$txtEmail">
            <div id="dmuklp_divEmail">
            </div>
        </div>
    </div>
    <div class="common_field_main">
        <div class="top_label">
            Post Code
        </div>
        <div class="top_input">
            <div id="postcode_lookup">
            </div>
        </div>
        <div style="display: none;" class="top_input">
            <input type="text" onkeypress="doClick('dmuklp_btnProceed',event)" placeholder="Enter postcode" autocomplete="Off" class="input_value1" id="dmuklp_TextBoxPostCode" name="dmuklp$TextBoxPostCode">
           
            
            <div id="dmuklp_divPostCode">
            </div>
        </div>
    </div>
    <div style="display: none;" id="divAddressLabel">
        <div class="common_field_main">
            <div class="top_label displaypc">
                &nbsp;</div>
            <div class="top_input">
                <span id="dmuklp_lblSelectAddress"></span>
                <div style="display: none" id="divAddressTextbox">
                    <select name="lstAddressIP" class="input_value" id="lstAddressIP">
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div style="display: none;" id="dmuklp_divaddressfields">
        <div class="common_field_main">
            <div class="top_label">
                Enter Address Line 1
            </div>
            <div class="top_input">
                <input type="text" onkeypress="doClick('dmuklp_btnProceed',event)" placeholder="Enter address line 1" autocomplete="Off" class="input_value" id="dmuklp_TextBoxHouseNumber" name="dmuklp$TextBoxHouseNumber">
                <div id="dmuklp_divHouseNumber">
                </div>
                
            </div>
        </div>
        <div class="common_field_main">
            <div class="top_label">
                 Enter Address Line 2
            </div>
            <div class="top_input">
                <input type="text" onkeypress="doClick('dmuklp_btnProceed',event)" placeholder="Enter address line 2" autocomplete="Off" class="input_value" id="dmuklp_TextBoxStreet" name="dmuklp$TextBoxStreet">
                <div id="dmuklp_divStreet">
                </div>
                
            </div>
        </div>
        <div class="common_field_main">
            <div class="top_label">
                Address Line 3
            </div>
            <div class="top_input">
                <input type="text" placeholder="Enter address line 3" autocomplete="Off" class="input_value" id="dmuklp_TextBoxStreet1" name="dmuklp$TextBoxStreet1">
                <div id="dmuklp_div1">
                </div>
                
            </div>
        </div>
        <div class="common_field_main">
            <div class="top_label">
                Town
            </div>
            <div class="top_input">
                <input type="text" onkeypress="doClick('dmuklp_btnProceed',event)" placeholder="Enter town" autocomplete="Off" class="input_value" id="dmuklp_TextBoxTown" name="dmuklp$TextBoxTown">
                <div id="dmuklp_divTown">
                </div>
                
            </div>
        </div>
        <div class="common_field_main">
            <div class="top_label">
                Country
            </div>
            <div class="top_input">
                <select class="input_value" disabled="disabled" id="dmuklp_ddlCountry" name="dmuklp$ddlCountry">
	<option value="GB" selected="selected">United Kingdom</option>

</select>
            </div>
        </div>
    </div>
    <div class="common_field_main">
        <div class="top_label displaypc">
            &nbsp;
        </div>
        <div class="top_input linehieght">
            By clicking the button below you will be accepting our <a href="/termsandconditions.aspx" target="_blank" class="underline">Terms &amp; Conditions</a> and our <a href="/privacy-policy.aspx" target="_blank" class="underline">Privacy Policy</a>
        </div>
    </div>
    <div class="common_field_main">
        <div class="top_label displaypc">
            &nbsp;
        </div>
        <div class="top_input">
            <!--<input type="submit" class="common_buttons" id="dmuklp_btnProceed" onclick="_gaq.push(['_trackEvent', 'SIM','Confirm', 'Free Sim', 100, true]);" value="I Want it Now" name="dmuklp$btnProceed">-->
			 <button type="button" class="common_buttons" id="bundles_submit"  value="I 
                Want it Now" name="dmuklp$btnProceed">I Want it Now </button>
        </div>
    </div>
</div>

            </div>