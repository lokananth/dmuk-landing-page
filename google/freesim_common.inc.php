<?php

/**
 * Function to API call using Curl lib
 * @param type $arr_param
 * @return type
 */
 
 define('DMUK_MYACCOUNT_FREESIM', 'http://192.168.2.102:9706/api/v1/FreeSimRegistration');

 function apiPost1($apiUrl, $data = Array(), $isAuth = true) {
              
                $header[0] = 'Authorization: mundiovectone F2SeWBiCUvVmO6z66C6qgXNAoHNe4YB9aIW3nK78zxw=';
                $header[1] = 'Mundio-Api-PublicKey: OTE3MjAwMDIyNTM4';
                $header[2] = 'Content-MD5:917200022538';
                $header[3] = 'Accept: application/json';
                $header[4] = 'Content-Type: application/json';
                $header[5] = 'crossDomain: true';
                if (!$data) {
                                $header[6] = 'Content-Length: 0';
                }
                
                //$header[0] = 'Authorization: mundiovectone 79wviZZxm5zhxaYfzB4x7B72hIMkXkk5d5fhLlP9mAs=';
                //$header[1] = 'Mundio-Api-PublicKey: MTAwMQ==';
                //$header[6] = 'Host: 192.168.2.102:9705';
                //$header[7] = 'Content-Length: 0';
                //$header[7] = 'Content-Length: ' . strlen(json_encode($header));
                //$header[3] = 'User-Agent: mundiovectone';
                //$header[4] = 'Accept-Charset: UTF-8';
                //print_r($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
   
   if ($data) {
        $str = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
    }

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $output = curl_exec($ch);
    curl_close($ch);
	
    return json_decode($output, true);
}

function freesim_curl_get_contents($arr_param) {

    $url = $arr_param['url'];
    // Initiate the curl session
    $ch = curl_init();
    // Set the URL
    curl_setopt($ch, CURLOPT_URL, $url);
    // Removes the headers from the output
    curl_setopt($ch, CURLOPT_HEADER, 0);
    // Return the output instead of displaying it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // Execute the curl session
    $output = curl_exec($ch);
    // Close the curl session
    curl_close($ch);
    // Return the output as a variable 

    return $output;
}

function freesim_curl_post_contents($arr_param) {/* echo '<pre>'; print_r($arr_param);*/
   
    $apiURL = $arr_param['url'];
    $apiValue = $arr_param['post_value'];
    header("HTTP/1.1 200 OK");
    header("Content-Type: application/json");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($apiValue));
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $output = curl_exec($ch);
    curl_close($ch);

    return json_decode($output, true);
}
